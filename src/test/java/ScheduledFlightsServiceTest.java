import FlightBooking.Repository.FlightRepository;
import FlightBooking.Services.FlightService;
import FlightBooking.model.FlightSearchCriteria;
import FlightBooking.model.FlightResponseDTO;
import FlightBooking.model.ScheduledFlights;
import FlightBooking.model.TravelClass;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ScheduledFlightsServiceTest {

    private FlightService flightService;
    private FlightRepository flightRepository;
    private LocalDate deptDate = LocalDate.now();

    @Before
    public void setUp() {
        flightRepository = mock(FlightRepository.class);
        flightService = new FlightService(flightRepository);
    }

    private List<ScheduledFlights> testData() {

        List<ScheduledFlights> flightsList = new ArrayList<>();
        ScheduledFlights flight1, flight2, flight3;

        flight1 = new ScheduledFlights(1, "Boeing 737", "Hyderabad", "Mumbai", deptDate.plusDays(1),
                new TravelClass(1, 10, 25000, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(1, 315, 13000, TravelClass.TravelType.BUSINESS_CLASS, 100),
                new TravelClass(1, 195, 6000, TravelClass.TravelType.ECONOMY_CLASS, 100));
        flight2 = new ScheduledFlights(2, "Airbus A320", "Hyderabad", "Texas", deptDate,
                new TravelClass(2, 0, 0, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(2, 0, 0, TravelClass.TravelType.BUSINESS_CLASS, 0),
                new TravelClass(2, 144, 4000, TravelClass.TravelType.ECONOMY_CLASS, 100));
        flight3 = new ScheduledFlights(3, "Airbus A321", "Hyderabad", "Sydney", deptDate,
                new TravelClass(3, 0, 0, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(3, 20, 10000, TravelClass.TravelType.BUSINESS_CLASS, 10),
                new TravelClass(3, 152, 5000, TravelClass.TravelType.ECONOMY_CLASS, 100));
        flightsList.add(flight1);
        flightsList.add(flight2);
        flightsList.add(flight3);
        return flightsList;
    }

    @Test
    public void returnAvailableFlights() {
        when(flightRepository.getAllFlights()).thenReturn(testData());
        Collection<ScheduledFlights> allFlightsList = flightService.getAllFlights();
        assertEquals(3, allFlightsList.size());
    }
    @Test
    public void returnFlightAvailableOn() {

        when(flightRepository.getAllFlights()).thenReturn(testData());
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Texas", 2, deptDate, TravelClass.TravelType.ECONOMY_CLASS);
        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        assertEquals(1, flightSearchResult.size());
    }
    @Test
    public void returnFlightAvailableBeforeTenDaysForFirstClass() {
        List<ScheduledFlights> flightsList = new ArrayList<>();
        ScheduledFlights flight1, flight2;

        flight1 = new ScheduledFlights(1, "Boeing 737", "Hyderabad", "Texas", deptDate.plusDays(9),
                new TravelClass(1, 10, 25000, TravelClass.TravelType.FIRST_CLASS, 2),
                new TravelClass(1, 315, 13000, TravelClass.TravelType.BUSINESS_CLASS, 100),
                new TravelClass(1, 195, 6000, TravelClass.TravelType.ECONOMY_CLASS, 100));
        flight2 = new ScheduledFlights(2, "Airbus A320", "Hyderabad", "Texas", deptDate,
                new TravelClass(2, 2, 1000, TravelClass.TravelType.FIRST_CLASS, 2),
                new TravelClass(2, 0, 0, TravelClass.TravelType.BUSINESS_CLASS, 0),
                new TravelClass(2, 144, 4000, TravelClass.TravelType.ECONOMY_CLASS, 100));

        flightsList.add(flight1);
        flightsList.add(flight2);
        when(flightRepository.getAllFlights()).thenReturn(flightsList);
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Texas", 2, deptDate.plusDays(9), TravelClass.TravelType.FIRST_CLASS);

        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        assertEquals(1, flightSearchResult.size());
    }
    @Test
    public void returnFlightsHavingSeats() {

        when(flightRepository.getAllFlights()).thenReturn(testData());
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 2, deptDate.plusDays(1), TravelClass.TravelType.ECONOMY_CLASS);

        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        assertEquals(1, flightSearchResult.size());
    }

    @Test
    public void returnFlightsMatchingOriginAndDestination() {
        when(flightRepository.getAllFlights()).thenReturn(testData());

        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 0, null, null);
        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        assertEquals(1, flightSearchResult.size());
    }

    @Test
    public void returnFlightsAvailableForEffectiveDepartureDate() {
        when(flightRepository.getAllFlights()).thenReturn(testData());
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 0, null, null);
        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        assertEquals(1, flightSearchResult.size());
    }

    @Test
    public void returnFlightsAvailableForEffectivePassengerCount() {
        when(flightRepository.getAllFlights()).thenReturn(testData());
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Texas", 0, deptDate, null);
        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        assertEquals(1, flightSearchResult.size());
    }

    @Test
    public void returnFlightsAvailableForEffectiveTravelType() {

        when(flightRepository.getAllFlights()).thenReturn(testData());
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Texas", 2, deptDate, null);
        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        assertEquals(1, flightSearchResult.size());
    }

    @Test
    public void EconomyClassFareCaluculator() {

        List<ScheduledFlights> flightsList = new ArrayList<>();
        ScheduledFlights flight1, flight2;

        flight1 = new ScheduledFlights(1, "Boeing 737", "Hyderabad", "Mumbai", deptDate.plusDays(1),
                new TravelClass(1, 10, 25000, TravelClass.TravelType.FIRST_CLASS, 2),
                new TravelClass(1, 315, 13000, TravelClass.TravelType.BUSINESS_CLASS, 100),
                new TravelClass(1, 195, 6000, TravelClass.TravelType.ECONOMY_CLASS, 195));
        flight2 = new ScheduledFlights(2, "Airbus A320", "Hyderabad", "Texas", deptDate,
                new TravelClass(2, 0, 0, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(2, 0, 0, TravelClass.TravelType.BUSINESS_CLASS, 0),
                new TravelClass(2, 144, 4000, TravelClass.TravelType.ECONOMY_CLASS, 100));

        flightsList.add(flight1);
        flightsList.add(flight2);

        when(flightRepository.getAllFlights()).thenReturn(flightsList);
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 2, null, TravelClass.TravelType.ECONOMY_CLASS);
        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        double totalFare = 0;
        for (FlightResponseDTO response : flightSearchResult) {
            totalFare = response.getTotalFare();
        }
        assertEquals(12000.0, totalFare, 0);
    }
    @Test
    public void returnFareConsideringDefaultClassAsEconomy() {

        List<ScheduledFlights> flightsList = new ArrayList<>();
        ScheduledFlights flight1, flight2;

        flight1 = new ScheduledFlights(1, "Boeing 737", "Hyderabad", "Mumbai", deptDate.plusDays(1),
                new TravelClass(1, 10, 25000, TravelClass.TravelType.FIRST_CLASS, 2),
                new TravelClass(1, 315, 13000, TravelClass.TravelType.BUSINESS_CLASS, 100),
                new TravelClass(1, 195, 6000, TravelClass.TravelType.ECONOMY_CLASS, 195));
        flight2 = new ScheduledFlights(2, "Airbus A320", "Hyderabad", "Texas", deptDate,
                new TravelClass(2, 0, 0, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(2, 0, 0, TravelClass.TravelType.BUSINESS_CLASS, 0),
                new TravelClass(2, 144, 4000, TravelClass.TravelType.ECONOMY_CLASS, 100));

        flightsList.add(flight1);
        flightsList.add(flight2);

        when(flightRepository.getAllFlights()).thenReturn(flightsList);
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 2, null, null);
        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        double totalFare = 0;
        for (FlightResponseDTO response : flightSearchResult) {
            totalFare = response.getTotalFare();
        }
        assertEquals(12000.0, totalFare, 0);
    }

    @Test
    public void returnEconomyClassWithOutExtraFareForFirstFortyPerSeatBookings() {
        List<ScheduledFlights> flightsList = new ArrayList<>();
        ScheduledFlights flight1, flight2;

        flight1 = new ScheduledFlights(1, "Boeing 737", "Hyderabad", "Mumbai", deptDate.plusDays(1),
                new TravelClass(1, 10, 25000, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(1, 315, 13000, TravelClass.TravelType.BUSINESS_CLASS, 100),
                new TravelClass(1, 195, 6000, TravelClass.TravelType.ECONOMY_CLASS, 117));
        flight2 = new ScheduledFlights(2, "Airbus A320", "Hyderabad", "Texas", deptDate,
                new TravelClass(2, 0, 0, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(2, 0, 0, TravelClass.TravelType.BUSINESS_CLASS, 0),
                new TravelClass(2, 144, 4000, TravelClass.TravelType.ECONOMY_CLASS, 100));

        flightsList.add(flight1);
        flightsList.add(flight2);
        when(flightRepository.getAllFlights()).thenReturn(flightsList);
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 2, null, TravelClass.TravelType.ECONOMY_CLASS);
        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        double totalFare = 0;
        for (FlightResponseDTO response : flightSearchResult) {
            totalFare = response.getTotalFare();
        }
        assertEquals(12000.0, totalFare, 0);
    }

    @Test
    public void returnEconomyClassWithThirtyPerExtraFareForNextFiftyPerSeatBookings() {
        when(flightRepository.getAllFlights()).thenReturn(testData());
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 2, null, TravelClass.TravelType.ECONOMY_CLASS);
        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        double totalFare = 0;
        for (FlightResponseDTO response : flightSearchResult) {
            totalFare = response.getTotalFare();
        }
        assertEquals(15600.0, totalFare, 0);
        //returns economyfare with extracharges when 100 seats are alreadybooked out of 195 seats.
    }

    @Test
    public void returnEconomyClassWithSixtyPerExtraFareForLastTenPerSeatBookings() {
        List<ScheduledFlights> flightsList = new ArrayList<>();
        ScheduledFlights flight1, flight2;

        flight1 = new ScheduledFlights(1, "Boeing 737", "Hyderabad", "Mumbai", deptDate.plusDays(1),
                new TravelClass(1, 10, 25000, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(1, 315, 13000, TravelClass.TravelType.BUSINESS_CLASS, 100),
                new TravelClass(1, 195, 6000, TravelClass.TravelType.ECONOMY_CLASS, 10));
        flight2 = new ScheduledFlights(2, "Airbus A320", "Hyderabad", "Texas", deptDate,
                new TravelClass(2, 0, 0, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(2, 0, 0, TravelClass.TravelType.BUSINESS_CLASS, 0),
                new TravelClass(2, 144, 4000, TravelClass.TravelType.ECONOMY_CLASS, 100));

        flightsList.add(flight1);
        flightsList.add(flight2);
        when(flightRepository.getAllFlights()).thenReturn(flightsList);
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 2, null, TravelClass.TravelType.ECONOMY_CLASS);
        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        double totalFare = 0;
        for (FlightResponseDTO response : flightSearchResult) {
            totalFare = response.getTotalFare();
        }
        assertEquals(19200.0, totalFare, 0);
    }

    @Test
    public void returnEconomyClassFareZeroWithoutEconomyClassSeats() {
        List<ScheduledFlights> flightsList = new ArrayList<>();
        ScheduledFlights flight1, flight2;

        flight1 = new ScheduledFlights(1, "Boeing 737", "Hyderabad", "Mumbai", deptDate.plusDays(1),
                new TravelClass(1, 10, 25000, TravelClass.TravelType.FIRST_CLASS, 2),
                new TravelClass(1, 315, 13000, TravelClass.TravelType.BUSINESS_CLASS, 100),
                new TravelClass(1, 195, 6000, TravelClass.TravelType.ECONOMY_CLASS, 0));
        flight2 = new ScheduledFlights(2, "Airbus A320", "Hyderabad", "Texas", deptDate,
                new TravelClass(2, 0, 0, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(2, 0, 0, TravelClass.TravelType.BUSINESS_CLASS, 0),
                new TravelClass(2, 144, 4000, TravelClass.TravelType.ECONOMY_CLASS, 100));

        flightsList.add(flight1);
        flightsList.add(flight2);

        when(flightRepository.getAllFlights()).thenReturn(flightsList);
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 2, deptDate, TravelClass.TravelType.ECONOMY_CLASS);
        Collection<FlightResponseDTO> flightResponseDTO = flightService.searchFlights(flightSearchCriteria);
        double economyClassFare = 0;
        for (FlightResponseDTO response : flightResponseDTO) {
            economyClassFare = response.getTotalFare();
        }
        assertEquals(0, economyClassFare, 0);
    }

    @Test
    public void BusinessClassFareCaluculator() {

        List<ScheduledFlights> flightsList = new ArrayList<>();
        ScheduledFlights flight1, flight2;

        flight1 = new ScheduledFlights(1, "Boeing 737", "Hyderabad", "Mumbai", deptDate.plusDays(1),
                new TravelClass(1, 10, 25000, TravelClass.TravelType.FIRST_CLASS, 2),
                new TravelClass(1, 315, 13000, TravelClass.TravelType.BUSINESS_CLASS, 100),
                new TravelClass(1, 195, 6000, TravelClass.TravelType.ECONOMY_CLASS, 100));
        flight2 = new ScheduledFlights(2, "Airbus A320", "Hyderabad", "Texas", deptDate,
                new TravelClass(2, 0, 0, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(2, 0, 0, TravelClass.TravelType.BUSINESS_CLASS, 0),
                new TravelClass(2, 144, 4000, TravelClass.TravelType.ECONOMY_CLASS, 100));

        flightsList.add(flight1);
        flightsList.add(flight2);

        when(flightRepository.getAllFlights()).thenReturn(flightsList);
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 2, null, TravelClass.TravelType.BUSINESS_CLASS);
        // double economyfare=flightService.searchFlights(flightSearchCriteria).
        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        double totalFare = 0;
        for (FlightResponseDTO response : flightSearchResult) {
            totalFare = response.getTotalFare();
        }
        assertEquals(26000.0, totalFare, 0);
    }

    @Test
    public void returnBusinessClassWithExtraFareOnMonday() {
        List<ScheduledFlights> flightsList = new ArrayList<>();
        ScheduledFlights flight1, flight2;

        TemporalField fieldISO = WeekFields.of(Locale.ENGLISH).dayOfWeek();
        deptDate = deptDate.with(fieldISO, 2);

        flight1 = new ScheduledFlights(1, "Boeing 737", "Hyderabad", "Mumbai", deptDate,
                new TravelClass(1, 10, 25000, TravelClass.TravelType.FIRST_CLASS, 2),
                new TravelClass(1, 315, 13000, TravelClass.TravelType.BUSINESS_CLASS, 100),
                new TravelClass(1, 195, 6000, TravelClass.TravelType.ECONOMY_CLASS, 100));
        flight2 = new ScheduledFlights(2, "Airbus A320", "Hyderabad", "Texas", deptDate,
                new TravelClass(2, 0, 0, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(2, 0, 0, TravelClass.TravelType.BUSINESS_CLASS, 0),
                new TravelClass(2, 144, 4000, TravelClass.TravelType.ECONOMY_CLASS, 100));

        flightsList.add(flight1);
        flightsList.add(flight2);

        when(flightRepository.getAllFlights()).thenReturn(flightsList);
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 2, deptDate, TravelClass.TravelType.BUSINESS_CLASS);

        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        double totalFare = 0;
        for (FlightResponseDTO response : flightSearchResult) {
            totalFare = response.getTotalFare();
        }
        assertEquals(36400.0, totalFare, 0);

    }

    @Test
    public void returnBusinessClassNoExtraFareOnTuesday() {
        List<ScheduledFlights> flightsList = new ArrayList<>();
        ScheduledFlights flight1, flight2;

        TemporalField fieldISO = WeekFields.of(Locale.ENGLISH).dayOfWeek();
        deptDate = deptDate.with(fieldISO, 3);

        flight1 = new ScheduledFlights(1, "Boeing 737", "Hyderabad", "Mumbai", deptDate.plusDays(1),
                new TravelClass(1, 10, 25000, TravelClass.TravelType.FIRST_CLASS, 2),
                new TravelClass(1, 315, 13000, TravelClass.TravelType.BUSINESS_CLASS, 100),
                new TravelClass(1, 195, 6000, TravelClass.TravelType.ECONOMY_CLASS, 100));
        flight2 = new ScheduledFlights(2, "Airbus A320", "Hyderabad", "Texas", deptDate,
                new TravelClass(2, 0, 0, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(2, 0, 0, TravelClass.TravelType.BUSINESS_CLASS, 0),
                new TravelClass(2, 144, 4000, TravelClass.TravelType.ECONOMY_CLASS, 100));

        flightsList.add(flight1);
        flightsList.add(flight2);

        when(flightRepository.getAllFlights()).thenReturn(flightsList);
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 2, deptDate.plusDays(1), TravelClass.TravelType.BUSINESS_CLASS);
        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        double totalFare = 0;
        for (FlightResponseDTO response : flightSearchResult) {
            totalFare = response.getTotalFare();
        }
        assertEquals(26000.0, totalFare, 0);

    }

    @Test
    public void returnsFirstClassFareZeroWithoutFirstClassSeats() {
        when(flightRepository.getAllFlights()).thenReturn(testData());
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 2, deptDate, TravelClass.TravelType.FIRST_CLASS);

        Collection<FlightResponseDTO> flightResponseDTO = flightService.searchFlights(flightSearchCriteria);
        double firstClassFare = 0;
        for (FlightResponseDTO response : flightResponseDTO) {
            firstClassFare = response.getTotalFare();
        }
        assertEquals(0, firstClassFare, 0);
    }

    @Test
    public void FirstClassFareCaluculator() {

        List<ScheduledFlights> flightsList = new ArrayList<>();
        ScheduledFlights flight1, flight2;

        flight1 = new ScheduledFlights(1, "Boeing 737", "Hyderabad", "Mumbai", deptDate.plusDays(1),
                new TravelClass(1, 10, 25000, TravelClass.TravelType.FIRST_CLASS, 2),
                new TravelClass(1, 315, 13000, TravelClass.TravelType.BUSINESS_CLASS, 100),
                new TravelClass(1, 195, 6000, TravelClass.TravelType.ECONOMY_CLASS, 100));
        flight2 = new ScheduledFlights(2, "Airbus A320", "Hyderabad", "Texas", deptDate,
                new TravelClass(2, 0, 0, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(2, 0, 0, TravelClass.TravelType.BUSINESS_CLASS, 0),
                new TravelClass(2, 144, 4000, TravelClass.TravelType.ECONOMY_CLASS, 100));

        flightsList.add(flight1);
        flightsList.add(flight2);

        when(flightRepository.getAllFlights()).thenReturn(flightsList);
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 2, null, TravelClass.TravelType.FIRST_CLASS);
        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        double totalFare = 0;
        for (FlightResponseDTO response : flightSearchResult) {
            totalFare = response.getTotalFare();
        }
        assertEquals(95000, totalFare, 0);
    }

    @Test
    public void returnsFirstClassFareWithTenPercentExtraFareEveryday() {

        List<ScheduledFlights> flightsList = new ArrayList<>();
        ScheduledFlights flight1, flight2;

        flight1 = new ScheduledFlights(1, "Boeing 737", "Hyderabad", "Mumbai", deptDate.plusDays(3),
                new TravelClass(1, 10, 25000, TravelClass.TravelType.FIRST_CLASS, 2),
                new TravelClass(1, 315, 13000, TravelClass.TravelType.BUSINESS_CLASS, 100),
                new TravelClass(1, 195, 6000, TravelClass.TravelType.ECONOMY_CLASS, 100));
        flight2 = new ScheduledFlights(2, "Airbus A320", "Hyderabad", "Texas", deptDate,
                new TravelClass(2, 0, 0, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(2, 0, 0, TravelClass.TravelType.BUSINESS_CLASS, 0),
                new TravelClass(2, 144, 4000, TravelClass.TravelType.ECONOMY_CLASS, 100));

        flightsList.add(flight1);
        flightsList.add(flight2);

        when(flightRepository.getAllFlights()).thenReturn(flightsList);
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria(
                "Hyderabad", "Mumbai", 2, deptDate.plusDays(3), TravelClass.TravelType.FIRST_CLASS);
        Collection<FlightResponseDTO> flightSearchResult = flightService.searchFlights(flightSearchCriteria);
        double totalFare = 0;
        for (FlightResponseDTO response : flightSearchResult) {
            totalFare = response.getTotalFare();
        }
        assertEquals(85000.0, totalFare, 0);
    }
}







