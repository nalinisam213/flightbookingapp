package FlightBooking.Repository;

import FlightBooking.model.ScheduledFlights;
import FlightBooking.model.TravelClass;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class FlightRepository {

    private Map<Integer, ScheduledFlights> flightMap;
    private LocalDate depDate = LocalDate.now();

    public FlightRepository() {
        flightMap = new HashMap<>();
        ScheduledFlights flight1, flight2, flight3,flight4,flight5;
        flight1 = new ScheduledFlights(1, "Boeing 737", "Hyderabad", "Mumbai", depDate.plusDays(1),
                new TravelClass(1, 10, 25000, TravelClass.TravelType.FIRST_CLASS, 10),
                new TravelClass(1, 315, 13000, TravelClass.TravelType.BUSINESS_CLASS, 100),
                new TravelClass(1, 195, 6000, TravelClass.TravelType.ECONOMY_CLASS, 93));
        flight2 = new ScheduledFlights(2, "Airbus A320", "Hyderabad", "Mumbai", depDate,
                new TravelClass(2, 0, 0, TravelClass.TravelType.FIRST_CLASS, 10),
                new TravelClass(2, 0, 0, TravelClass.TravelType.BUSINESS_CLASS, 20),
                new TravelClass(2, 144, 4000, TravelClass.TravelType.ECONOMY_CLASS, 100));
        flight3 = new ScheduledFlights(3, "Airbus A321", "Hyderabad", "Sydney", depDate,
                new TravelClass(3, 0, 0, TravelClass.TravelType.FIRST_CLASS, 0),
                new TravelClass(3, 20, 10000, TravelClass.TravelType.BUSINESS_CLASS, 10),
                new TravelClass(3, 152, 5000, TravelClass.TravelType.ECONOMY_CLASS, 100));
        flight4 = new ScheduledFlights(4, "Boeing 727", "Hyderabad", "Mumbai", depDate.plusDays(2),
                new TravelClass(4, 10, 28000, TravelClass.TravelType.FIRST_CLASS, 10),
                new TravelClass(4, 315, 16000, TravelClass.TravelType.BUSINESS_CLASS, 100),
                new TravelClass(4, 195, 6000, TravelClass.TravelType.ECONOMY_CLASS, 93));
        flight5 = new ScheduledFlights(5, "Airbus A330", "Hyderabad", "Mumbai", depDate.plusDays(3),
                new TravelClass(5, 0, 29000, TravelClass.TravelType.FIRST_CLASS, 10),
                new TravelClass(5, 0, 18000, TravelClass.TravelType.BUSINESS_CLASS, 20),
                new TravelClass(5, 144, 4000, TravelClass.TravelType.ECONOMY_CLASS, 100));
        flightMap.put(flight1.getFlight_id(), flight1);
        flightMap.put(flight2.getFlight_id(), flight2);
        flightMap.put(flight3.getFlight_id(), flight3);
        flightMap.put(flight4.getFlight_id(), flight4);
        flightMap.put(flight5.getFlight_id(), flight5);
    }

    public Collection<ScheduledFlights> getAllFlights() {
        return flightMap.values();
    }
}
