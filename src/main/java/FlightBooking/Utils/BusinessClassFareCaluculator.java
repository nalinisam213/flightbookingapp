package FlightBooking.Utils;
import FlightBooking.model.FlightSearchCriteria;
import FlightBooking.model.ScheduledFlights;
import java.time.DayOfWeek;
import java.time.LocalDate;

public class BusinessClassFareCaluculator implements IFlightFareCaluculator {
   private ScheduledFlights scheduledFlight;
   private FlightSearchCriteria flightSearchCriteria;

   public BusinessClassFareCaluculator(ScheduledFlights scheduledFlight, FlightSearchCriteria flightSearchCriteria) {
        this.scheduledFlight = scheduledFlight;
        this.flightSearchCriteria = flightSearchCriteria;
    }
    @Override
    public double calculateFare() {
        double extraFare = 0;
        LocalDate departureDate = flightSearchCriteria.getEffectiveDepartureDate();
        int passengerCount = flightSearchCriteria.getEffectivePassengerCount();
        double businessBaseFare = scheduledFlight.getBusinessClass().getBaseprice();
        DayOfWeek dayOfWeek = departureDate.getDayOfWeek();
        if (dayOfWeek == DayOfWeek.MONDAY || dayOfWeek == DayOfWeek.FRIDAY || dayOfWeek == DayOfWeek.SUNDAY) {
            extraFare = 0.4 * businessBaseFare;
        }
        return (businessBaseFare + extraFare) * passengerCount;
    }
}
