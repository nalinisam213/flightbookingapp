package FlightBooking.Utils;

import FlightBooking.model.FlightSearchCriteria;
import FlightBooking.model.ScheduledFlights;
import java.time.LocalDate;
import static java.time.temporal.ChronoUnit.DAYS;

public class FirstClassFareCaluculator implements IFlightFareCaluculator {

    private ScheduledFlights scheduledFlight;
    private FlightSearchCriteria flightSearchCriteria;

    public FirstClassFareCaluculator(ScheduledFlights scheduledFlight, FlightSearchCriteria flightSearchCriteria) {
        this.scheduledFlight = scheduledFlight;
        this.flightSearchCriteria = flightSearchCriteria;
    }
    @Override
    public double calculateFare() {
        double extraFare;
        int passengerCount = flightSearchCriteria.getEffectivePassengerCount();
        double firstBaseFare = scheduledFlight.getFirstClass().getBaseprice();
        long numOfDays = DAYS.between(LocalDate.now(), flightSearchCriteria.getEffectiveDepartureDate());
        extraFare = firstBaseFare * 0.1 * (10 - numOfDays);
        return (firstBaseFare + extraFare) * passengerCount;
    }
}
