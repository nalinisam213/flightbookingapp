package FlightBooking.Utils;

import FlightBooking.model.FlightSearchCriteria;
import FlightBooking.model.ScheduledFlights;

public class EconomyClassFareCaluculator implements IFlightFareCaluculator {

   private ScheduledFlights scheduledFlight;
    private FlightSearchCriteria flightSearchCriteria;

    public EconomyClassFareCaluculator(ScheduledFlights scheduledFlight, FlightSearchCriteria flightSearchCriteria) {
        this.scheduledFlight = scheduledFlight;
        this.flightSearchCriteria = flightSearchCriteria;
    }
    @Override
    public double calculateFare() {
        int availableEconomySeats = scheduledFlight.getEconomyClass().getAvailableseats();
        int totalEconomySeats = scheduledFlight.getEconomyClass().getSeats();
        double economyBaseFare = scheduledFlight.getEconomyClass().getBaseprice();
        int passengerCount = flightSearchCriteria.getEffectivePassengerCount();
        double extraFare = 0;

        int first40PercentSeatsMark = (int) (totalEconomySeats * 0.6);
        int last10PercentSeatsMark = (int) (totalEconomySeats * 0.1);

        if (availableEconomySeats > first40PercentSeatsMark) {
            extraFare = 0;
        } else if ((availableEconomySeats < first40PercentSeatsMark) && (availableEconomySeats > last10PercentSeatsMark)) {
            extraFare = economyBaseFare * 0.3;
        } else if ((availableEconomySeats < last10PercentSeatsMark) && (availableEconomySeats >= 1)) {
            extraFare = economyBaseFare * 0.6;
        }
        return (economyBaseFare + extraFare) * passengerCount;
    }
}
