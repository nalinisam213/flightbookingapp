package FlightBooking.Services;
import FlightBooking.Repository.FlightRepository;
import FlightBooking.Utils.BusinessClassFareCaluculator;
import FlightBooking.Utils.EconomyClassFareCaluculator;
import FlightBooking.Utils.FirstClassFareCaluculator;
import FlightBooking.Utils.IFlightFareCaluculator;
import FlightBooking.model.FlightSearchCriteria;
import FlightBooking.model.FlightResponseDTO;
import FlightBooking.model.ScheduledFlights;
import FlightBooking.model.TravelClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class FlightService {
    private FlightRepository flightRepository;
    private IFlightFareCaluculator flightFare;

    @Autowired
    public FlightService(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }

    public Collection<ScheduledFlights> getAllFlights() {
        return flightRepository.getAllFlights();
    }
    public Collection<FlightResponseDTO> searchFlights(FlightSearchCriteria flightSearchCriteria) {
        return getAllFlights().stream()
                .filter(scheduledFlight -> scheduledFlight.getOrigin().equals(flightSearchCriteria.getOrigin()) &&
                        scheduledFlight.getDestination().equals(flightSearchCriteria.getDestination()) &&
                        isFlightAvailableOn(scheduledFlight, flightSearchCriteria.getEffectiveDepartureDate(), flightSearchCriteria.getEffectiveTravelType()) &&
                        isFlightHasSeats(scheduledFlight, flightSearchCriteria.getEffectivePassengerCount(), flightSearchCriteria.getEffectiveTravelType()))
                .map(scheduledFlight -> flightResponse(scheduledFlight, flightSearchCriteria))
                .collect(Collectors.toList());
    }
    private boolean isFlightAvailableOn(ScheduledFlights scheduledFlights, LocalDate departureDate, TravelClass.TravelType travelType) {
        if (scheduledFlights.getDepartureDate().equals(departureDate)) {
            if (travelType == TravelClass.TravelType.FIRST_CLASS) {
                return Period.between(LocalDate.now(), departureDate).getDays() < 10;
            } else
                return true;
        }
        return false;
    }
    private boolean isFlightHasSeats(ScheduledFlights scheduledFlights, int passengerCount, TravelClass.TravelType travelType) {
        if (travelType == TravelClass.TravelType.FIRST_CLASS) {
            return (scheduledFlights.getFirstClass().getAvailableseats() >= passengerCount);
        } else if (travelType == TravelClass.TravelType.BUSINESS_CLASS) {
            return (scheduledFlights.getBusinessClass().getAvailableseats() >= passengerCount);
        }
        return (scheduledFlights.getEconomyClass().getAvailableseats() >= passengerCount);
    }
    private double calculateTotalFare(ScheduledFlights scheduledFlight, FlightSearchCriteria flightSearchCriteria) {
        TravelClass.TravelType travelType = flightSearchCriteria.getEffectiveTravelType();
        if (travelType == TravelClass.TravelType.ECONOMY_CLASS) {
            flightFare = new EconomyClassFareCaluculator(scheduledFlight,flightSearchCriteria);
        } else if (travelType == TravelClass.TravelType.BUSINESS_CLASS) {
            flightFare = new BusinessClassFareCaluculator(scheduledFlight,flightSearchCriteria);
        } else {
           flightFare = new FirstClassFareCaluculator(scheduledFlight,flightSearchCriteria);
        }
        return flightFare.calculateFare();
    }
    private FlightResponseDTO flightResponse(ScheduledFlights scheduledFlights, FlightSearchCriteria flightSearchCriteria) {
        FlightResponseDTO flightResponseDTO = new FlightResponseDTO();
        flightResponseDTO.setFlightId(scheduledFlights.getFlight_id());
        flightResponseDTO.setFlightname(scheduledFlights.getFlight_name());
        flightResponseDTO.setOrigin(scheduledFlights.getOrigin());
        flightResponseDTO.setDestination(scheduledFlights.getDestination());
        flightResponseDTO.setPassengerCount(flightSearchCriteria.getEffectivePassengerCount());
        flightResponseDTO.setDepartureDate(flightSearchCriteria.getEffectiveDepartureDate());
        flightResponseDTO.setTravelType(flightSearchCriteria.getEffectiveTravelType());
        flightResponseDTO.setTotalFare(calculateTotalFare(scheduledFlights, flightSearchCriteria));
        return flightResponseDTO;
    }
}
