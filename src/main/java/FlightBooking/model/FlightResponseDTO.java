package FlightBooking.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FlightResponseDTO {
    private int flightId;
    private String flightname;
    private String origin;
    private String destination;
    private int passengerCount;
    private LocalDate departureDate;
    private TravelClass.TravelType travelType;
    private double totalFare;

}
