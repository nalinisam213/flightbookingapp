package FlightBooking.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ScheduledFlights {

    private int flight_id;
    private String flight_name;
    private String origin;
    private String destination;
    private LocalDate departureDate;
    private TravelClass firstClass;
    private TravelClass businessClass;
    private TravelClass economyClass;

}