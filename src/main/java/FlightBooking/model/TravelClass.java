package FlightBooking.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class TravelClass {

    private int flight_id;
    private int seats;
    private float baseprice;
    private TravelType travelType;
    private int availableseats;

    public enum TravelType {
        FIRST_CLASS,
        BUSINESS_CLASS,
        ECONOMY_CLASS
    }
}
