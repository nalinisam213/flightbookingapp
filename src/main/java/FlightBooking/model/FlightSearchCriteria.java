package FlightBooking.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FlightSearchCriteria {
    private String origin;
    private String destination;
    private int passengerCount;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate departureDate;
    private TravelClass.TravelType travelType;

    public int getEffectivePassengerCount() {
        int passengerCount = this.getPassengerCount();
        return (passengerCount == 0) ? 1 : passengerCount;
    }

    public LocalDate getEffectiveDepartureDate() {
        LocalDate departureDate = this.getDepartureDate();
        LocalDate now = LocalDate.now();
        return (departureDate == null) ? now.plusDays(1) : departureDate;
    }

    public TravelClass.TravelType getEffectiveTravelType() {
        return (this.getTravelType() == null) ? TravelClass.TravelType.ECONOMY_CLASS : this.getTravelType();
    }
}
