package FlightBooking.Controller;

import FlightBooking.Services.FlightService;
import FlightBooking.model.FlightResponseDTO;
import FlightBooking.model.FlightSearchCriteria;
import FlightBooking.model.ScheduledFlights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/flights")
public class FlightController {
    @Autowired
    private FlightService flightService;

    @RequestMapping("/all")
    public Collection<ScheduledFlights> getallFlights() {
        return flightService.getAllFlights();
    }

    @RequestMapping("/search")

    public Collection<FlightResponseDTO> searchflights(FlightSearchCriteria flightSearchCriteria) {
        return flightService.searchFlights(flightSearchCriteria);
    }
}

