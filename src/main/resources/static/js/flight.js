$(document).ready(function () {
     $("#flightsTable").hide();
    $("#search-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();
        fire_ajax_submit();
    });
});

function makeTable(data) {
    var rows = '';
    $.each(data, function(rowIndex, r) {
        var row = "<tr>";
        row += "<td>"+r.flightname+"</td>";
        row += "<td>"+r.origin+"</td>";
        row += "<td>"+r.destination+"</td>";
        row += "<td>"+r.departureDate+"</td>";
        row += "<td>"+r.travelType+"</td>";
        row += "<td>"+r.passengerCount+"</td>";
        row += "<td>"+r.totalFare+"</td>";
        row += "</tr>";
        rows += row;
    });
    return rows;
}
function fire_ajax_submit() {

    $("#btn-search").prop("disabled", true);
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/flights/search",
        data: {
            origin: $("#origin").val(),
            destination: $("#destination").val(),
            passengerCount: $("#passengerCount").val(),
            departureDate: $("#departureDate").val(),
            travelType: $("#travelType").val()
        },
        dataType: 'json',
        timeout: 600000,
        success: function (response)
        {
                    $("#flightsTable").show();
                    if(response!=null && response.length>0)
                    {
                        var rows = makeTable(response);
                        $('#flightRows').html(rows);
                    }
                    else
                    {
                        $("#flightRows").text('No flights found').css('font-weight', 'bold');;
                    }

        },
        error: function (e) {

                    var json = "<h4>Flight Search Response</h4><pre>"
                        + e.responseText + "</pre>";
                    $('#SearchResult').html(json);

                    console.log("ERROR : ", e);
                    $("#btn-search").prop("disabled", false);

                }
    });

}
