import React, { Component } from 'react';

class FlightSearch extends Component {
    state = { 
        isLoading : true,
        FlightSearchResponse : []
     }

    async componentDidMount()
    {
        const response = await fetch('/flights/all');
        const body = await response.json();
        console.log(body);
        this.setState({FlightSearchResponse:body , isLoading: false });
    }
    render() { 
        const { isLoading , FlightSearchResponse} = this.state;
        if(isLoading)
            return (<div>Loding ...</div>)
        return ( 
          <div>
              <h2>Flights Response</h2>
              {
                 FlightSearchResponse.map( flight => 
                    <div id={flight.flight_id}>
                        {flight.flight_name}
                        {flight.origin}
                        {flight.destination}
                        {flight.departureDate}
                    </div>
                 )
              }
          </div>
         );
    }
}
export default FlightSearch;